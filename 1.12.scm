#lang sicp

;;               x
;;      | 0  1  2  3  4  5
;;    __|_________________
;;    0 | 1  
;;    1 | 1  1
;;  y 2 | 1  2  1
;;    3 | 1  3  3  1
;;    4 | 1  4  6  4  1
;;    5 | 1  5 10 10  5  1


(define (pascals-triangle x y)
  (cond
    ((or (< x 0) (< y 0) (> x y)) 0)
    ((or (= x 0)(= x y)) 1)
    (else
     (+ (pascals-triangle (- x 1) (- y 1)) (pascals-triangle x (- y 1))))))


(pascals-triangle 2 4)
(pascals-triangle 2 5)