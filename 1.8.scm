#lang sicp

(define (abs x)
  (cond ((< x 0) (- x))
        (else x)))

(define (cube x)
  (* x x x))

(define (square x)
  (* x x))

(define (good-enough? guess x)
  (< (abs (- (cube guess) x)) 0.00000001))

(define (average x y)
  (/ (+ x y) 2))

(define (improve guess x)
  (average guess (/ (+ (/ x (square guess)) (* 2 guess))
                    3)))

(define (cubert-iter guess x)
  (if (good-enough? guess x)
      guess
      (cubert-iter (improve guess x) x)))

(define (cubert x)
  (cubert-iter 1.0 x))

(cubert 10)