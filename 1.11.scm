#lang sicp

(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1))
         (* 2 (f (- n 2)))
         (* 3 (f (- n 3))))))

(f 3)
(f 4)
(f 5)
(f 10)
(f 30)

(define (f2 n)
  (define (f-iter count a b c)
    (cond
      ((= count 0) a)
      (else
       (f-iter (- count 1)
               b
               c
               (+ c (* 2 b) (* 3 a))))))
  (f-iter n 0 1 2))

(f2 3)
(f2 4)
(f2 5)
(f2 10)
(f2 30)
