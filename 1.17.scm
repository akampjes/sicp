#lang sicp

(define (double x)
  (+ x x))

(define (even? x)
  (= (remainder x 2) 0))

(define (halve x)
  (if (even? x)
      (/ x 2)
      x))

(define (fast-multiply a b)
  (cond ((= b 0) 0)
        ((even? b) (double (fast-multiply a (halve b))))
        (else (+ a (fast-multiply a (- b 1))))))

(fast-multiply 10 1)
(fast-multiply 10 2)
(fast-multiply 10 3)
(fast-multiply 3 7)
(fast-multiply 4 7)
(fast-multiply 2 10)