#lang sicp

(define (even? n)
  (= (remainder n 2) 0))

(define (square n)
  (* n n))

(define (fast-expt-ittr a b n)
  (cond ((= n 1) a)
        ((even? n) (fast-expt-ittr (* a (square b)) b (/ n 2)))
        (else (fast-expt-ittr (* a b) b (- n 1)))))

(fast-expt-ittr 1 23 5)