#lang sicp

(define (double x)
  (+ x x))

(define (even? x)
  (= (remainder x 2) 0))

(define (halve x)
  (if (even? x)
      (/ x 2)
      x))

(define (fast-multiply-ittr acc a b)
  (cond ((= b 0) acc)
        ((even? b) (fast-multiply-ittr acc (double a) (halve b)))
        (else (fast-multiply-ittr (+ acc a) a (- b 1)))))

(define (fast-multiply a b)
  (fast-multiply-ittr 0 a b))

(fast-multiply 10 1)
(fast-multiply 10 2)
(fast-multiply 10 3)
(fast-multiply 3 7)
(fast-multiply 4 7)
(fast-multiply 2 10)