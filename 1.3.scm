#lang sicp

(define (square x)
  (* x x))

(define (sum-of-squares x y)
  (+ (square x) (square y)))

(define (max x y)
  (if (> x y) x y))

(define (2-largest x y z)
  (if (> x y)
      (+ (square x) (if (> y z)(square y)(square z)))
      (+ (square y) (if (> x z)(square x)(square z)))))

(2-largest 7 11 3)
